# PiHole Einrichtung

![Framework](https://img.shields.io/badge/Framework-mdBook-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-GNU%20FDL%201.3-orange)

Dieses Repository enthält ein mdBook, das eine detaillierte Anleitung zur Einrichtung von PiHole bereitstellt. Die ursprüngliche Anleitung wurde von [TotallyLeGIT](https://codeberg.org/TotallyLeGIT) erstellt.

## Inhaltsverzeichnis

- [Über PiHole Einrichtung](#über-pihole-einrichtung)
- [Verwendung](#verwendung)
- [Beitragen](#beitragen)
- [Kontakt](#kontakt)
- [Lizenz](#lizenz)

## Über PiHole Einrichtung

PiHole ist ein Netzwerkwächter und Anzeigenblocker, der auf einem Raspberry Pi oder einem anderen Linux-basierten System installiert werden kann. Dieses Repository enthält ein mdBook, das eine Schritt-für-Schritt-Anleitung zur Einrichtung von PiHole auf einem Raspberry Pi oder einem ähnlichen Gerät bietet.

Die Anleitung deckt verschiedene Aspekte ab, wie die Installation des Betriebssystems, die Konfiguration des Netzwerks, das Einrichten von PiHole und die Durchführung grundlegender Konfigurationsschritte. Es wird auch auf erweiterte Funktionen und mögliche Anpassungen eingegangen.

## Verwendung

Um die Anleitung zur Einrichtung von PiHole zu verwenden, folgen Sie bitte den folgenden Schritten:

1. Stellen Sie sicher, dass Sie die erforderliche Hardware besitzen, insbesondere einen Raspberry Pi oder ein ähnliches Linux-basiertes System.

2. Klonen Sie das Repository mit Git:
```
git clone https://codeberg.org/Anleitungen/PiHole-einrichtung.git
```

3. Navigieren Sie in das Verzeichnis des geklonten Repositories:
```
cd PiHole-einrichtung
```

4. Installieren Sie die Abhängigkeiten für das mdBook:
```
# Beispiel für Ubuntu oder Debian
sudo apt install mdbook
```

5. Starten Sie das mdBook, um die Anleitung lokal anzuzeigen:
```
mdbook serve
```

Die Anleitung wird nun auf `http://localhost:3000` bereitgestellt. Öffnen Sie Ihren bevorzugten Webbrowser und navigieren Sie zu dieser Adresse, um die Anleitung anzuzeigen.

## Beitragende

Beiträge zu dieser PiHole Einrichtungsanleitung sind willkommen! Wenn Sie Verbesserungen vornehmen möchten, neue Abschnitte hinzufügen oder Fehler korrigieren möchten, führen Sie bitte die folgenden Schritte aus:

1. Forken Sie das Repository.
2. Erstellen Sie einen neuen Branch für Ihre Änderungen.
3. Führen Sie Ihre Änderungen durch und veröffentlichen Sie diese in Ihrem Fork.
4. Erstellen Sie einen Pull-Request, um Ihre Änderungen in das Hauptrepository einzubringen.

Wir schätzen Ihre Beiträge und werden sie überprüfen und zusammenführen, um die PiHole Einrichtungsanleitung kontinuierlich zu verbessern.

## Kontakt

Bei Fragen oder Anliegen können Sie uns über die Kontaktdaten in diesem Repository erreichen.

## Lizenz

Die PiHole Einrichtungsanleitung steht unter der [GNU Free Documentation License](LICENSE). Bitte lesen Sie die Lizenzdatei für weitere Informationen.
