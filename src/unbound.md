Unbound & Hyperlocal

[Unbound](https://docs.pi-hole.net/guides/unbound/)

[Hyperlocal ≙ DNS-Root-Zone lokal in Unbound vorhalten](https://datatracker.ietf.org/doc/rfc8806/)

Wir sind wieder per SSH mit dem Pi verbunden.

```
ssh 192.168.1.5
```

Die Installation Unbounds ist ein Kinderspiel. Zudem installieren wir noch man (steht für manual – Handbuch), um mit den Befehlen man unbound und man unbound.conf später nachschlagen zu können, wie Unbound funktioniert und konfiguriert wird. Doch nicht nur zu Unbound, sondern nahezu allem gibt es eine sog. man page, die man mit man XYZ aufrufen kann und eine gute erste Anlaufstelle für Fragen und Unklarheiten ist.

```
apt install unbound man
```

Das bejahen wir natürlich. Der Unbound-Service wird zunächst nicht starten können, da der Standard-Port 53, den Unbound verwenden will, schon durch Pi-hole belegt ist. Das und weitere Einstellungen gehen wir mit der Konfigurationsdatei an.

```
nano /etc/unbound/unbound.conf.d/server.conf
```

Eine ganze Menge soll dort hinein:

```
server:
	# If no logfile is specified, syslog is used
	#logfile: "/var/log/unbound/unbound.log"
	log-time-ascii: yes
	verbosity: 1

	port: 5335

	# for *native* IPv6 ISP connection you may set this to yes
	prefer-ip6: no

	# where to find root server data
	root-hints: /usr/share/dns/root.hints

	# Capitalization randomization: set to "no" on errors
	use-caps-for-id: yes

	# Perform prefetching of close to expired message cache entries
	# This only applies to domains that have been frequently queried
	prefetch: yes

	# fetch DS records earlier (DNSSEC): more cpu usage, less latency
	prefetch-key: yes

	# One thread should be sufficient, can be increased on beefy machines
	num-threads: 1

	# increase cache size to utilize more RAM
	msg-cache-size: 32m
	rrset-cache-size: 64m

	# Ensure privacy of local IP ranges
	private-address: 192.168.0.0/16
	private-address: 172.20.0.0/16
	private-address: 169.254.0.0/16
	private-address: 172.16.0.0/12
	private-address: 10.0.0.0/8
	private-address: fd00::/8
	private-address: fe80::/10


remote-control:
	control-enable: yes


# get data for all TLDs by IXFR (or AXFR) from root servers
# these are the only servers that answer an IXFR query
auth-zone:
	name: "."
	primary: 199.9.14.201       # b.root-servers.net
	primary: 192.33.4.12        # c.root-servers.net
	primary: 192.112.36.4       # g.root-servers.net
	primary: 2001:500:200::b    # b.root-servers.net
	primary: 2001:500:2::c      # c.root-servers.net
	primary: 2001:500:12::d0d   # g.root-servers.net
	fallback-enabled: yes
	for-downstream: no
	for-upstream: yes
	zonefile: /var/lib/unbound/root.zone
```

Wie immer: speichern und schließen. Der Teil auth-zone in der obigen Konfiguration ist dazu da, damit wir uns die jeweils erste DNS-Anfrage an die DNS-Root-Server durch Unbound sparen. Unbound muss dadurch gar nicht erst nachfragen, welche Server bspw. für .de oder .org zuständig sind, sondern hat die Daten stets parat und hält sie per Abgleich mit den Root-Servern aktuell.
Nachdem wir Unbound neugestartet haben – dies sollte nun fehlerfrei klappen –, wollen wir das Ganze natürlich auch auf Funktionalität überprüfen; erst einfache DNS-Anfragen, dann DNSSEC.

```
systemctl restart unbound
dig kuketz-blog.de @127.0.0.1 -p 5335 +short
dig sigfail.verteiltesysteme.net @127.0.0.1 -p 5335
dig sigok.verteiltesysteme.net @127.0.0.1 -p 5335
```

Wenn alles funktioniert, liefert die erste dig-Anfrage nur eine IP-Adresse zurück (aktuell 185.163.119.132). Das zweite dig enthält u. A. status: SERVFAIL und das letzte status: NOERROR und flags: […] ad (d. h. DNSSEC ist für diese Anfrage valide). Nun loggen wir uns wieder im Pi-hole-Webinterface ein und stellen Unbound als DNS-Resolver im Feld Custom 3 unter Settings → DNS ein, indem wir das Häkchen setzen und ::1#5335 eintragen. Bei Custom 1 und 2 werden die Häkchen entfernt. Ganz unten speichern wir die Einstellungen (Save).
Unbound und Pi-hole haben beide einen eingebauten Cache. Einer reicht jedoch aus, daher deaktivieren wir den von Pi-hole. Dazu müssen wir mehrere Dateien bearbeiten (immer anschließend speichern). Erstens:

```
nano /etc/pihole/setupVars.conf
```

Dort suchen und ändern:

```
CACHE_SIZE=0
```

Und zweitens:

```
nano /etc/dnsmasq.d/01-pihole.conf
```

Auch dort nur umändern:

```
cache-size=0
```

Eine weitere Einstellung, die uns erlaubt, das Ergebnis der von Unbound durchgeführten DNSSEC-Validierung an anfragende Clients weiterzuleiten, setzen wir in einer separaten Datei:

```
nano /etc/dnsmasq.d/10-pihole-extra.conf
```

Dort fügen wir ein:

```
proxy-dnssec
```

Abschließend noch ein Neustart.

```
reboot
```

## Geschafft! Oder doch nicht?

Du hast nun die derzeit größtmögliche Selbstbestimmung und Freiheit in Bezug auf das Domain Name System erlangt. Doch sind wir jetzt für alle Zeit mit dem Projekt fertig? Nicht ganz, denn ein IT-System erfordert immer auch Pflege. Daher sollten wir uns einmal die Woche per SSH mit dem Pi verbinden und dort Updates einspielen. Das lässt sich einfach nebenbei erledigen, denn normalerweise muss hier weniger Arbeit, sondern vielmehr Zeit investiert werden, doch auch das in überschaubarem Maße: Ein wöchentliches Update sollte auch mit langsamem Pi-Modell und SD-Karte nicht mehr als wenige Minuten dauern. Die Anweisungen dafür kann man in einer Befehlszeile verketten:

```
apt update && apt upgrade && apt autoremove && pihole -up
```

Sind Updates fällig, ist während des Vorgangs noch eine Bestätigung notwendig. Es gibt auch die Möglichkeit, das Programm Unattended Upgrades zu verwenden, welches Sicherheitsupdates automatisch einspielt. Da ich jedoch ein Freund von Anwesenheit bei Updates bin, werde ich dies nicht weiter behandeln.

Fassen wir einmal zusammen, was wir erreicht haben: Unsere Geräte im Heimnetzwerk fragen nun stets unseren Raspberry Pi nach den IP-Adressen, die sich hinter Domainnamen wie www.kuketz-blog.de verbergen. Der Pi filtert zunächst Anfragen an Werbe-Domains heraus. Für alle anderen Anfragen kontaktiert unser Pi nur noch die entsprechenden autoritativen Nameserver. Davon gibt es mehrere, sodass wir nicht (mehr) von einer einzigen zentralen Instanz abhängig sind. Unbound unternimmt für uns außerdem weitere Schritte, um Anfragen nach außen zu minimieren – so zum Beispiel das Zwischenspeichern von Antworten auf DNS-Anfragen oder das Hyperlocal genannte vorhalten der Rootzonendaten.

Die komplette Einrichtung geht im Grunde locker von der Hand. Jetzt kann sich niemand mehr damit herausreden, die Installation wäre zu schwierig. Also: Richtet euch einen eigenen DNS-Server ein und macht das Internet ein Stück dezentraler, um der Machtkonzentration entgegenzuwirken.

Spannend bleibt außerdem, was aus den [Bestrebungen der IETF](https://tools.ietf.org/id/draft-bortzmeyer-dprive-resolver-to-auth-00.html) wird, die Verschlüsselung der DNS-Anfragen an die autoritativen Server zu ermöglichen. Auch wenn die Entwicklung noch nicht abzuschätzen ist und eingeschlafen wirkt, werden wir diese verfolgen und entsprechende Schritte in die Tat umsetzen, sobald die Möglichkeit dazu besteht.
