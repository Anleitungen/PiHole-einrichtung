# Pi-hole

Weiter geht's per SSH auf dem Pi.

```
ssh 192.168.1.5
```

Da das Image recht minimalistisch gehalten ist, müssen wir selbst ein Tool wie [cURL](https://de.wikipedia.org/wiki/CURL) erst manuell installieren. Darüber hinaus installiere ich hier gern noch ein Hilfswerkzeug. bash-completion vervollständigt viele Befehle, wenn man nur den Anfang eintippt und dann Tab drückt.

```
apt update && apt install curl bash-completion
```

Danach können wir uns das Installations-Skript für Pi-hole herunterladen.

```
curl -sSL "https://install.pi-hole.net" > install-pi-hole
```

An dieser Stelle empfehle ich, wenigstens kurz in die Datei reinzuschauen, ob etwas Sinnvolles heruntergeladen wurde.

```
less install-pi-hole
```

Mit q kann man die Betrachtung beenden. Danach folgt die eigentliche Pi-hole-Installation. Der Befehl dafür (erste Zeile) und die vorzunehmenden Einstellungen, die währenddessen vom Script erfragt werden, (restliche Zeilen) sehen so aus:

```
bash install-pi-hole

Upstream DNS Provider: Custom
Enter your desired upstream DNS provider(s): 5.1.66.255, 91.239.100.100
Die Blockierliste aktiviert lassen (OK)
IPv4/IPv6: beides aktiv lassen (OK)
Webinterface: On; Web Server: On; Log: On; Privacy Mode: 0 Show everything (Das ist schließlich nur für uns privat.)
```

Die hier gewählten DNS-Server werden vom [Freifunk München](https://ffmuc.net/wiki/doku.php?id=knb:dns) und [UncensoredDNS](https://blog.uncensoreddns.org/dns-servers/) betrieben. Nach der Installation entfernen wir das Script wieder, da es seinen Dienst getan hat. Anschließend stellen wir für das Web-Interface ein sichereres Passwort ein.

```
rm install-pi-hole
pihole -a -p
```

An der Pi-hole-Konfiguration ändern wir noch eine Kleinigkeit, um die SD-Karte etwas zu schonen. Dazu bearbeiten wir diese Datei:

```
nano /etc/pihole/pihole-FTL.conf
```

und fügen dort das Folgende an:

```
#DNS-Anfragen x Tage speichern
MAXDBDAYS=30
#alle x Minuten pihole-FTL.db von RAM auf SD-Karte schreiben
DBINTERVAL=60
```

Den systemeigenen Resolver können wir deaktivieren, da wir nun unseren eigenen betreiben.

```
systemctl disable systemd-resolved
```

Aufgrund der ganzen Änderungen am System ein Neustart.

```
reboot
```

Nun machen wir über das Web-Interface weiter. Wir geben im Browser folgende Adresse ein und loggen uns unter "Login" mit dem vorhin vergebenen Passwort ein.

```
192.168.1.5/admin
```

Wir fügen dort für ordentliches Werbe-(und weiteres) Blocking ein paar Blockier-/Host-Listen hinzu. Möglich ist das unter Group Management → Adlists: Ich füge hier die [Non-crossed-Liste der Blocklisten von Firebog](https://v.firebog.net/hosts/lists.php?type=nocross) hinzu (Selbstbeschreibung: "For when someone is usually around to whitelist falsely blocked sites"). Je nach gewünschtem Grad des (möglichen Over-)Blockings könnte man auch eine andere Liste wählen. Auf [derselben Seite](https://v.firebog.net/hosts/lists.php) werden auch noch je eine striktere und eine sanftere, aber immer noch gut filternde Liste angeboten. Die Listen markieren wir allesamt und fügen sie in das Textfeld ein; als Comment bspw. firebog.net no-cross angeben. Anschließend navigieren wir noch zu Tools → Update Gravity und klicken dort auf Update, damit sich Pi-hole alle Listen herunterlädt und die Domains in die eigene Datenbank aufnimmt. Je nach Pi-Modell kann das einige Minuten dauern, also an dieser Stelle gern einen Kaffee kochen.

Auf dem Pi-hole-Dashboard sollte die Zahl der blockierten Domains mit einigen hunderttausend angegeben werden. Nun können wir den Pi auch schon als lokalen DNS-Server nutzen. Dazu öffnen wir die Router-Einstellungen und stellen beim DHCP(-Server) ein, dass alle Geräte den Pi bzw. dessen IP-Adresse (`192.168.1.5`) für DNS-Anfragen nutzen sollen. Das Vorgehen dabei variiert auch hier je nach Router, daher wieder nur für die Fritz!Box: Die IP-Adresse ist an zwei Stellen einzutragen. Erstens unter Heimnetz → Netzwerk → Karteireiter Netzwerkeinstellungen → weitere Einstellungen ausklappen → IPv4-Einstellungen → Lokaler DNS-Server. Dies ist der Resolver für die DHCP-Clients, also Geräte, die ihre IP-Adresse vom Router zugewiesen bekommen. Zudem zweitens unter Internet → Zugangsdaten → Karteireiter DNS-Server → Andere DNSv4-Server verwenden in beiden Feldern. Diese sind für die Anfragen der Fritz!Box selbst – und damit übrigens auch für das Gastnetz.

Für IPv6 würde ich die öffentliche Adresse wählen, welche mit `fd` beginnt, diese sieht z. B. so aus: `fd00:0:0:0:affe:cafe:abba:fefe`. Diese muss einerseits an derselben Stelle wie beim vorigen Zweitens eingetragen werden. Andererseits noch unter Heimnetz → Netzwerk → Karteireiter Netzwerkeinstellungen → weitere Einstellungen ausklappen → IPv6-Einstellungen → DNSv6-Server im Heimnetz. Zudem sollten im Regelfall Häkchen bei Router Advertisement im LAN aktiv und bei DNSv6-Server auch über Router Advertisement bekanntgeben (RFC 5006) gesetzt sein. Näher kann ich in diesem Tutorial nicht auf IPv6 eingehen, da das völlig ausufern würde.

Übrigens sind DNS-Anfragen unabhängig von der zur internen Vernetzung genutzten IP-Adresse für beide Adressversionen (v6/v4) möglich.
