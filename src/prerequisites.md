# Raspberry Pi einrichten

## Vorab

Ich empfehle den Betrieb des Raspberry Pis mit Netzwerkkabel.

Die Anleitung wurde für Linux erstellt, auf anderen Systemen sind Anpassungen höchstens in der ersten Hälfte dieses Beitrags nötig (Kommandos mit vorangestelltem $).

Am Ende jedes Beitrags ist ein größerer Schritt abgeschlossen und der Raspberry Pi in diesem Stadium funktionsfähig. An diesen Stellen sind Unterbrechungen passend – egal ob für längere Pausen oder weil man nur Teile der Anleitung als Vorlage für ein anderes Projekt heranzieht.

## SD-Karte vorbereiten

Zuerst holen wir uns das [aktuelle Debian stable für den Pi](https://raspi.debian.net/tested-images/); derzeit (bis 2023) Debian 11 Bullseye. Klickt den Link, sucht dort nach eurem Pi-Modell und ladet die zugehörigen drei Dateien hinter den Links xz-compressed image, sha256sum und GPG-signed sha256sum herunter. In meinem Fall lautet einer der Dateinamen 20210823_raspi_2_bullseye.img.xz. Je nach Datum und Pi-Modell werden diese jedoch variieren – passt die Befehle bitte entsprechend an.

Hinweis: Alle Varianten des Raspberry Pi 4 erfordern dasselbe Image. Zwar gibt es in der Übersicht zwei Zeilen dazu, diese stehen jedoch nur für verschiedene getestete Geräte. Die Dateien sind dieselben. Das Gleiche gilt für die Modellreihe Raspberry Pi 0/1.

Im Downloadverzeichnis öffnen wir ein Terminal. Als Erstes importieren wir den OpenPGP-Schlüssel und überprüfen die Authentizität und Integrität der Prüfsummen.

```
gpg2 --keyserver keyserver.ubuntu.com --recv-keys E2F63B4353F45989
```

```
gpg --verify 20210823_raspi_2_bullseye.img.xz.sha256.asc
```

Wenn alles stimmt, lautet eine Zeile der Ausgabe so:

gpg: Korrekte Signatur von […]

Der Rest der Ausgabe warnt u. A., weil keine [Vertrauenskette](https://en.wikipedia.org/wiki/Chain_of_trust) (chain of trust) von uns zum Schlüssel der signierenden Person führt. Das müssen wir an dieser Stelle so hinnehmen. Wir überprüfen noch, ob die Prüfsumme auch zu dem Image gehört und entpacken anschließend das Image.

```
sha256sum -c 20210823_raspi_2_bullseye.img.xz.sha256
20210823_raspi_2_bullseye.img.xz: OK
unxz 20210823_raspi_2_bullseye.img.xz
```

Es gilt nun herauszufinden, welche Bezeichnung die SD-Karte hat. Anhand des Befehls lsblk können wir diese ermitteln, für andere Infos kann man auch die Option -f anhängen (lsblk -f). Eine einfache Methode ist, den Befehl zweimal auszuführen: erst vor, dann nach Anstöpseln der SD-Karte. Ich gebe den Bezeichner vorsichtshalber mit /dev/sdX an, damit nicht blind kopiert wird – in meinem Fall lautet er /dev/sdc, bei euch kann das anders sein. Das Image schreiben wir anschließend auf die SD-Karte, /dev/sdX ersetzen wir entsprechend mit dem gefundenen Bezeichner. **Seid wirklich vorsichtig und schaut lieber dreimal hin**, was ihr bei of=… eingebt, sonst macht ihr im Zweifel das System platt.

```
lsblk
sudo dd bs=4M conv=fsync status=progress if=20210823_raspi_2_bullseye.img of=/dev/sdX
```

Nachdem der Vorgang abgeschlossen ist, bietet es sich an, die SD-Karte vorsichtshalber ab- und wieder anzustecken, um Fehlern vorzubeugen. Um später sofort eine SSH-Verbindung zum Pi aufbauen zu können, müssen wir jetzt noch ein paar Schritte am Rechner erledigen. Wir erstellen uns ein Schlüsselpaar. Dank [Passwort-Manager](https://www.kuketz-blog.de/keepassxc-auto-type-und-browser-add-on-im-alltag-nutzen-passwoerter-teil1/) können wir ein starkes Passwort vergeben. Die anderen Felder dürfen ruhig leer bleiben.

```
ssh-keygen -t ed25519 -a 100 -f ~/.ssh/id_ed25519_pidns
```

Wir hängen die SD-Karte temporär mit unseren gewöhnlichen Rechten am Rechner ein – an das Ersetzen von sdX denken –, …

```
sudo mkdir -p /media/SD
sudo mount -o uid="$(id -u $USER)" /dev/sdX1 /media/SD
```

… um den eben generierten öffentlichen Schlüssel in einer Datei zu hinterlegen. Zuerst die komplette Ausgabe des folgenden Befehls kopieren, …

```
cat ~/.ssh/id_ed25519_pidns.pub
```

dann die Datei bearbeiten, …

```
nano /media/SD/sysconf.txt
```

folgende Zeile suchen und wie folgt ändern (Die am Zeilenanfang entfernen!):

```
root_authorized_key=Kopiertes-hier-einfügen
```

Speichern und schließen. Bei Nano geht das, indem wir Strg+x drücken und die Nachfrage beim Speichern mit y (Englisch) oder j (Deutsch) bestätigen. Die vorigen Schritte des Einhängens und Bearbeitens dürfen natürlich auch gern über den Dateimanager erfolgen. Die SD-Karte wird wieder ausgehängt.

```
sudo umount /dev/sdX1
```

Karte abziehen, in den Pi stecken, Pi anstöpseln und los. :-)

## Einrichtung Debians und des SSH-Zugangs

Im Router geben wir dem Pi per DHCP eine feste IP-Adresse. Ich wähle hier die 192.168.1.5, je nach eurer Netzwerkausgestaltung müsst ihr evtl. eine andere wählen. Auch kann ich das an dieser Stelle nicht abschließend für jeden Router erklären, da das stets verschieden einzurichten ist – Aber es gibt sicher Anleitungen zu eurem Router im Web. Bei einer Fritz!Box bspw. ist das im Webinterface unter Heimnetz → Netzwerk im Karteireiter Netzwerkverbindungen zu finden, indem man das entsprechende Gerät bearbeitet (Stift-Symbol). Dort kann die letzte (vierte) Zahl der IP-Adresse geändert und ein Haken bei Diesem Netzwerkgerät immer die gleiche IP-Adresse zuweisen gesetzt werden.
**Die hier eingestellte IP-Adresse müsst ihr in der Anleitung stets entsprechend ersetzen, falls ihr eine andere vergeben habt.**

Damit die Adressänderung wirksam wird, kann man z. B. das Netzwerkkabel des Pis abziehen und wieder anstecken. Falls das nicht klappt, den Router und/oder Pi einfach mal neustarten.

Die SSH-Config auf unserem PC ändern wir so, dass wir schnell und einfach per SSH auf den Pi kommen, weil automatisch der passende Accountname und Schlüssel ausgewählt wird.

```
nano ~/.ssh/config
```

Folgendes fügen wir an, danach speichern und schließen:

```
Host 192.168.1.5
    IdentitiesOnly yes
    IdentityFile ~/.ssh/id_ed25519_pidns
    User root
```

Vom Pi erfragen wir nun den Fingerabdruck des Hostschlüssels. Dieser dient als eindeutiges Identifikationsmerkmal des Servers und ist ein Sicherheitsmerkmal von SSH, um Angriffe auf die Verbindung leichter erkennen zu können. Der Fingerabdruck wird auf unserem Rechner gehasht an die Datei angefügt, welche solche Kennungen sammelt.

```
ssh-keyscan -Ht ed25519 192.168.1.5 >> ~/.ssh/known_hosts
```

Wir initiieren eine SSH-Sitzung auf dem Pi.

```
ssh 192.168.1.5
```

Ab jetzt geht es auf dem Pi weiter. Als erstes stellen wir die passende Zeitzone ein.

```
timedatectl set-timezone Europe/Berlin
```

Die bei jedem Boot notwendige Zeitsynchronisation lassen wir über die [NTP-Server von dismail.de](https://dismail.de/info.html#ntp) vornehmen. Dazu fügen wir in einer Datei …

```
nano /etc/systemd/timesyncd.conf
```

… die IP-Adressen in die entsprechende Zeile ein (und entfernen wieder die #).

NTP=213.136.94.10 80.241.218.68 78.46.223.134

Datei speichern und schließen. Ein starkes Root-Passwort darf nicht fehlen.

```
passwd
```

Und auch der SSH-Server soll noch ein wenig abgesichert werden. Dazu bearbeiten wir eine Konfigurationsdatei …

```
nano /etc/ssh/sshd_config.d/custom.conf
```

… und fügen dort folgende Zeilen ein:

```
AllowStreamLocalForwarding no
AllowTcpForwarding no
AllowUsers root
ClientAliveInterval 600
PasswordAuthentication no
X11Forwarding no
```

Um unsere SD-Karte etwas zu schonen, stellen wir das System-Log so ein, dass es ausschließlich in den Arbeitsspeicher schreibt. Zwar sind die Logs dann nach einem Neustart verloren, jedoch erhöhen wir so die Lebenszeit unserer SD-Karte, da damit ein wesentlicher Teil der Schreibvorgänge eliminiert wird. Der Vorgang ist natürlich reversibel (einfach Datei wieder löschen, z. B. für Fehlersuche) und wir planen ohnehin nicht, den Pi oft neuzustarten. Nach dem Erstellen eines Verzeichnisses legen wir eine neue Datei an.

```
mkdir /etc/systemd/journald.conf.d
nano /etc/systemd/journald.conf.d/00-volatile-storage.conf
```

In diese kommen die notwendigen Konfigurationen.

```
[Journal]
Storage=volatile
RuntimeMaxUse=30
```

Als nächstes folgt ein vollständiges Update des Systems (Bestätigung erforderlich).

```
apt update && apt full-upgrade
```

Abschließend installieren wir noch ein Paket, welches für [Lokalisierung](https://en.wikipedia.org/wiki/Internationalization_and_localization) (Anpassung von Sprache, Maßeinheiten etc.) erforderlich ist.

```
apt install locales
```

Zur Einrichtung der Lokalisierungen nehmen wir diesen Befehl

```
dpkg-reconfigure locales
```

Dort suchen wir die folgenden zwei Zeilen heraus und aktivieren diese mit der Leertaste (Aktivierung an \* erkennbar).

```
de_DE.UTF-8 UTF-8
en_US.UTF-8 UTF-8
```

Mit Enter geht es weiter. Nun können wir noch eine Standard-Lokalisierung auswählen. Die obigen sind beide in Ordnung, je nachdem ob man lieber Englisch oder Deutsch hat – anvisieren und bestätigen. Nach kurzer Wartezeit noch ein abschließender Neustart und wir sind mit der Einrichtung fertig.

```
reboot
```

**Disclaimer:** Das ist keine Anleitung dazu, wie man einen Pi perfekt absichert. Hängt dieser hinter einem Router und es wurden keine Port-Weiterleitungen eingerichtet (Wenn du eine hättest, wüsstest du es vermutlich.), ist dies in meinen Augen jedoch hinreichend. Wichtig sind regelmäßige Updates – siehe dazu auch den Punkt Geschafft! Oder doch nicht? im Teil [Unbound & Hyperlocal](./unbound.md).
