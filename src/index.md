# Vorwort

## Einführung

Im Internet von heute haben wir zunehmend mit riesigen, zentralen Entitäten bzw. Firmen mit Monopolstellung zu kämpfen. Diese akkumulieren immer mehr Daten, was gelinde gesagt gefährlich werden kann. Was gäbe es da Besseres, als diesem Trend eigenhändig ein Stück weit entgegenwirken zu können und für ein wenig mehr Dezentralität und persönliche Autonomie zu sorgen?

Auf dem Kuketz-Blog und im dazugehörigen Forum gibt es bereits viele Informationen zur digitalen Selbstbestimmung. Zum Beispiel wird nahegelegt, einen vertrauenswürdigen DNS-Server zu verwenden. Das ist ein erster Schritt, jedoch ändert es nichts daran, dass wir dieser Instanz vertrauen müssen. Alle Entitäten, die wir im Internet kontaktieren (Webseiten, E-Mail etc.), bekommen meist nur einen kleineren Abriss von uns zu sehen. Der DNS-Server jedoch erfährt nahezu alle Stellen, die wir per Internet kontaktieren (Ausnahme: Anonymisierungsdienste wie Tor; VPN jedoch ausdrücklich nicht). Wir wollen uns im Folgenden nun auch dieser zentralen Stelle entledigen und uns ein Stück freier und das Internet ein Stück dezentraler machen, indem wir einfach selbst einen DNS-Server aufsetzen.

## Was haben wir vor?

Dies ist eine Schritt-für-Schritt-Anleitung von der ersten Einrichtung eines Raspberry Pis bis zu dem Punkt, an welchem der Pi alle DNS-Anfragen aus unserem Netzwerk beantwortet. Dies erreichen wir, indem wir uns des üblicherweise für alle Anfragen genutzten externen DNS-Servers entledigen und diesen stattdessen einfach selbst betreiben (Unbound). Die DNS-Anfragen werden dann je nach Inhalt auf verschiedene autoritative Server (Das sind die, die auch dem "gewöhnlichen" DNS-Server die Daten liefern.) verteilt. Zudem wenden wir eine Technik an (Hyperlocal), um die Anzahl der DNS-Anfragen, die unser Heimnetz verlassen, zu reduzieren, um möglichst wenige Datenspuren zu hinterlassen. Wer es genau wissen will, findet mehr dazu in der zweiten Hälfte der Fragen & Antworten. Darüber hinaus sollen ausgehende DNS-Anfragen an Werbe-Domains geblockt werden (Pi-hole), wobei auch das unter unserer Kontrolle stehen wird und nach Belieben eingerichtet werden kann. Neben der Problemstellung ist eine grundsätzliche Erklärung gegeben und auch jeder Schritt kommentiert. Denn das Ganze soll nicht nur nachgeahmt, sondern auch verstanden werden.

## Mitwirkende

Erstellt von [Tschaeggaer](https://codeberg.org/TotallyLeGIT), erste veröffentlichung im [Kuketz-Forum](https://forum.kuketz-blog.de/viewtopic.php?p=85240)

Übertragen in [mdBook](https://rust-lang.github.io/mdBook/) von [Tealk](https://codeberg.org/Tealk)
