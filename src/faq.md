Fragen & Antworten

#Frequently Asked Questions (FAQ)

**[F]** Aber beim DNS-Server X ist alles mit [DNS over TLS](https://de.wikipedia.org/wiki/DNS_over_TLS) (DoT) oder [DNS over HTTPS](https://de.wikipedia.org/wiki/DNS_over_HTTPS) (DoH) verschlüsselt und keiner sieht meine Anfragen. Hier ist alles unverschlüsselt!

**[A]** Auch beim DNS-Server X sind die Anfragen nur von uns bis zum Server verschlüsselt. Dieser entschlüsselt die Anfragen und hat diese im Klartext vorliegen. Übrigens erhält er auch unverschlüsselt seine Daten von den autoritativen Servern. Die zentrale Instanz mit der kompletten Kenntnis aller Anfragen bliebe also – nämlich der DNS-Server X. Andere Entitäten dazwischen, bspw. der Internetprovider ([ISP](https://de.wikipedia.org/wiki/Internetdienstanbieter)), sehen allerdings die DNS-Anfragen nicht mehr, soweit richtig. Doch kontaktieren wir direkt nach der DNS-Anfrage ohnehin meist die entsprechende Stelle. Selbst wenn dies verschlüsselt wäre, stünde die Ziel-IP-Adresse im Klartext in den Kopf-Daten der Pakete. Da können wir DNS-Anfragen verschlüsseln wie wir wollen, wir Posaunen unser Ziel anschließend sowieso raus. Der Informationsgehalt ist dabei nicht exakt derselbe, aber vergleichbar. Hier ist also kein Blumentopf zu gewinnen. Es ist derzeit auch schlicht nicht möglich, die Anfragen an die autoritativen Server zu verschlüsseln. Es gab u. A. schon eine [Bestrebung in diese Richtung](https://tools.ietf.org/id/draft-bortzmeyer-dprive-resolver-to-auth-00.html) durch die IETF, allerdings sollten wir uns für die nahe Zukunft [nicht allzu große Hoffnungen](https://root-servers.org/media/news/Statement_on_DNS_Encryption.pdf) machen. Vertraulichkeit ist demzufolge nicht das beste Argument. Integrität der DNS-Daten schaffen DoT oder DoH auch nicht, da wir damit nicht überprüfen können, ob der DNS-Server überhaupt korrekte Daten losschickt, sondern nur, ob die Daten auf dem Weg zu uns nicht verändert wurden.

---

**[F]** Ein Online-DNS-Test sagt mir nach Einrichtung, ich würde einen DNS-Server meines Internet-Anbieters nutzen!?

**[F]** Der Test gibt den Hostnamen <kennung>.dip0.t-ipconnect.de, <kennung>.dyn.telefonica.de etc. oder meine eigene IP-Adresse aus, ist das korrekt?

**[A]** Wenn man die IP-Adresse des Tests unter die Lupe nimmt, ergibt sich die Antwort. Schaut bspw. bei [nsupdate.info](https://www.nsupdate.info/myip) nach der eigenen IP-Adresse. Ist das dieselbe? Wahrscheinlich schon. Und welcher DNS-Server soll benutzt werden? Genau: Unser Unbound, welches hinter unserem Router an unserem Anschluss arbeitet und daher auch unsere IP-Adresse hat. Alles super also! Der o. g. Hostname ist nur eine Kennung, die der Provider dem eigenen Anschluss zuweist.
Hinweis: Ich habe schon erlebt, dass [nsupdate.info](https://www.nsupdate.info) in Blocklisten vorkam und so durch Pi-hole blockiert wurde. Dann muss man diese Domain vorher per Webinterface in die Allowlist (Erlaubliste) aufnehmen.

---

**[F]** Nach dem Upgrade vom alten Tutorial (ohne komplette Neuinstallation) oder mit Raspberry Pi OS gibt es Probleme.

**[A]** Du hast damit die Pfade dieser Anleitung auf eigene Gefahr verlassen. Dennoch: Schau bitte zunächst, ob [diese Beiträge](https://forum.kuketz-blog.de/viewtopic.php?p=88547#p88547) dein Problem lösen.

---

**[F]** Ich will alles wissen!

**[A]** Na gut, du hast es so gewollt:

**Begriffe**

- [DNS Terminology](https://tools.ietf.org/html/rfc8499)
- Resource Record (RR): Jede valide Anfrage an einen DNS-Server wird mit einem Resource Record beantwortet. Das kann eine IP-Adresse (A oder AAAA Record) sein, aber auch ein Verweis auf einen weiteren DNS-Server, der genauere Informationen liefern kann (NS), einen alternativen Namen für eine Domäne (CNAME) und noch viel mehr.
- iterativer Resolver: ein DNS-Server, der Anfragen entgegennimmt und mit einem RR antwortet. Häufig sind das Verweise auf andere Server (NS RR), die Genaueres wissen, anstatt der gesuchten Antwort.
- rekursiver Resolver: ein DNS-Server, der Anfragen entgegennimmt, eigenständig zu Ende bearbeitet und die vom Endgerät gesuchte Antwort (den gewünschten Resource Record) zurückliefert. Dazu müssen meist iterative Resolver befragt werden, nicht selten auch mehrere hintereinander. Der rekursive Resolver ist meist mit dem Wort DNS-Server gemeint.
  Name-/DNS-Server: iterativer oder rekursiver Resolver
- autoritativer Server: iterativ arbeitender Resolver, der von sich aus Wissen über eine DNS-Zone hat, ohne andere Resolver fragen zu müssen
- DNS-Zone: Ein Teil des hierarchisch aufgebauten DNS, implementiert als Konfiguration eines authoritativen Resolvers. Diese Konfiguration enthält das Wissen darüber, wo die Daten aller direkt untergeordneter Zonen zu finden sind. Z. B. kennen die Server der Root-Zone (.) alle Server, die für die Top-Level-Domains (TLDs: .de., .com., .org. usw.) zuständig sind. Zu jeder (inkl. nicht-vollständigen) Domäne gehört auch eine DNS-Zone.
- Root-Server: die Server an der Spitze der Hierarchie des DNS, verantwortlich für die Root-Zone (.)

## Wie muss man sich DNS-Anfragen vorstellen?

Schematisch könnte man eine übliche DNS-Anfrage z. B. nach www.kuketz-blog.de so darstellen ([ein ausführlicheres Schaubild auf Wikipedia](https://upload.wikimedia.org/wikipedia/commons/3/3c/DNS-query-to-wikipedia.svg)):

```
PC: "IP-Adresse für www.kuketz-blog.de.?"
  → rekursiver Resolver
–––
  rekursiver Resolver: "Wie ist die IP-Adresse für www.kuketz-blog.de.?"
    ← Server der Zone .: "<IP-Adresse des Servers zur Zone .de.>"
  rekursiver Resolver: "Wie ist die IP-Adresse für www.kuketz-blog.de.?"
    ← Server der Zone .de.: "<IP-Adresse des Servers zur Zone kuketz-blog.de.>"
  rekursiver Resolver: "Wie ist die IP-Adresse für www.kuketz-blog.de.?"
    ← Server der Zone kuketz-blog.de.: "123.45.67.89"
–––
rekursiver Resolver: "123.45.67.89"
  → PC
```

Nach dem Tutorial wird das Ganze so aussehen:

```
PC: "IP-Adresse für www.kuketz-blog.de.?"
  → Pi
–––
  Pi: "Wie ist die IP-Adresse für kuketz-blog.de.?"
    ← Server der Zone .de.: "<IP-Adresse des Servers zur Zone kuketz-blog.de.>"
  Pi: "Wie ist die IP-Adresse für www.kuketz-blog.de.?"
    ← Server der Zone kuketz-blog.de.: "123.45.67.89"
–––
Pi: "123.45.67.89"
  → PC
```

Anmerkung: Das Wissen über die Zone kuketz-blog.de. liegt aktuell auf einem Netcup-Server.

Wie wir sehen, treten in diesem Fall nur noch zwei externe Stellen auf – noch weniger Kontakt zu anderen Stellen ist derzeit technisch nicht möglich. Unser Pi bzw. das darauf laufende Programm Unbound kontaktiert also nur noch die wenigen passenden autoritativen Server direkt. Je nach Anfrage können sich die angerufenen Server unterscheiden. Somit sind wir nicht mehr von einem einzigen rekursiven Resolver abhängig, der alles von uns erfährt, sondern verteilen die Anfragen an verschiedene autoritative Server. Zudem versucht Unbound, jedem Server möglichst wenig zu verraten. Im Beispiel erkennen wir das daran, dass der Pi dem Server der Zone .de. nicht verrät, dass er auf die Subdomain www. des Kuketz-Blogs will. Als weitere Technik kommt zum Einsatz, dass der Pi DNS-Auflösungen zwischenspeichert, sogenanntes [Caching](https://de.wikipedia.org/wiki/Caching). Die zu den Adressen gehörigen IP-Adressen werden dabei eine gewisse Zeit auf dem Pi vorgehalten, [da es wahrscheinlich ist](https://de.wikipedia.org/wiki/Lokalit%C3%A4tseigenschaft#Zeitliche_Lokalit%C3%A4t), dass sie zeitnah nochmals erfragt werden. So wird die Anzahl der Anfragen an nicht unter unserer Kontrolle stehende Instanzen noch weiter verringert.
