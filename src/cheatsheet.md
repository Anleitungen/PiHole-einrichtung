Spickzettel

Für Personen, die die Anleitung zum wiederholten Male durchführen wollen und daher die Erklärungen nicht mehr benötigen, gibt es hier den Spickzettel. Dieser beinhaltet kompakt alle Befehle und Dateiinhalte sowie die erforderlichen URLs. Längere Dateiinhalte sind zur Erkennbarkeit vorne und hinten mit ——— begrenzt, diese Trennung bitte nicht kopieren.

```
https://raspi.debian.net/tested-images/

gpg2 --keyserver keyserver.ubuntu.com --recv-keys E2F63B4353F45989
gpg --verify 20210823_raspi_2_bullseye.img.xz.sha256.asc

gpg: Good signature from […]

sha256sum -c 20210823_raspi_2_bullseye.img.xz.sha256

20210823_raspi_2_bullseye.img.xz: OK

unxz 20210823_raspi_2_bullseye.img.xz
lsblk
sudo dd bs=4M conv=fsync status=progress if=20210823_raspi_2_bullseye.img of=/dev/sdX
ssh-keygen -t ed25519 -a 100 -f ~/.ssh/id_ed25519_pidns
sudo mkdir -p /media/SD
sudo mount /dev/sdX1 /media/SD
cat ~/.ssh/id_ed25519_pidns.pub
sudoedit /media/SD/sysconf.txt

root_authorized_key=cat-Ausgabe-hier-einfügen

sudo umount /dev/sdX1
nano ~/.ssh/config

Host 192.168.1.5
    IdentitiesOnly yes
    IdentityFile ~/.ssh/id_ed25519_pidns
    User root

ssh-keyscan -Ht ed25519 192.168.1.5 >> ~/.ssh/known_hosts
ssh 192.168.1.5

timedatectl set-timezone Europe/Berlin
```
